# 安新宇
## 2021.9.15
1. 文章阅读
 - [防抖节流](https://juejin.cn/post/6844903736196726798)
2. 刷题
 - [分发饼干](https://leetcode-cn.com/problems/assign-cookies/)
 - [重复的子字符串](https://leetcode-cn.com/problems/repeated-substring-pattern/)
3. 项目进度
- [x] 搜索页面
## 2021.9.14
1. 文章阅读
- [美团大佬解读的vuex源码](https://tech.meituan.com/2017/04/27/vuex-code-analysis.html)
2. leecode 刷题
- [z字变形](https://leetcode-cn.com/problems/zigzag-conversion/)
3. 源码解读
## 2021.9.13
1. 文章阅读
- [宏烨找房](https://www.sqfcw.com/m/)
2. leecode 刷题
- [计算质数](https://leetcode-cn.com/problems/count-primes/)
- [阶乘后的零](https://leetcode-cn.com/problems/factorial-trailing-zeroes/)
## 2021.9.12
1. 文章阅读
 - [if 我是前端团队 Leader，怎么制定前端协作规范?](https://juejin.cn/post/6844903897610321934)
2. leecode 刷题
 - [第三大的数](https://leetcode-cn.com/problems/third-maximum-number/)
 - [字符串相加](https://leetcode-cn.com/problems/add-strings/)
## 20209.10
 1. 文章阅读
 - [http和https的区别](https://mp.weixin.qq.com/s/UE7Zw0aSbxLuFFSraSUIOQ)
 2. 算法题
 - [全排列 II](https://leetcode-cn.com/problems/permutations-ii/)
 3. 项目进度
 - [x] 修改账户的用户名
 - [x] 修改账户的密码
 4. 源码阅读
## 2020.9.9
 1. 文章阅读
 - [MobX 上手指南](https://juejin.cn/post/6844903653757698062)
 2. 算法题
 - [电话号码的字母组合](https://leetcode-cn.com/problems/letter-combinations-of-a-phone-number/submissions/)
 3. 项目进度
 - [x] 实现支付功能
 4. 源码阅读
## 2020.9.8
 1. 文章阅读
 - [关于csrf](https://juejin.cn/post/6844903653757698062)
 2. 算法题
 - [组合总和](https://leetcode-cn.com/problems/combination-sum/submissions/)
 3. 项目进度
 - [x] 后端发送邮件
 - [x] 页面管理模块
 4. 源码阅读
## 2020.9.7
 1. 文章阅读
 - [前端内存泄漏处理](https://juejin.cn/post/7005110828593020965)
 2. 算法题
 - [罗马数字转整数](https://leetcode-cn.com/problems/roman-to-integer/comments/)
 3. 项目进度
 - [x] 海报页面排版
 4. 源码阅读
## 2020.9.6
 1. 文章阅读
 - [JavaScripts基础](https://juejin.cn/post/6948679834218283022)
 2. 算法题
 - [字符串中的第一个唯一字符](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn5z8r/)
 3. 项目进度
 - [x] 封装面包屑组件
 - [x] 修改用户名
 4. 源码阅读
## 2020.9.3
 1. 文章阅读
 - [前端浏览器缓存知识梳理](https://juejin.cn/post/6947936223126093861)
 2. 算法题
 - [两数之和](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2jrse/)
 - [最后一个单词的长度](https://leetcode-cn.com/problems/length-of-last-word/)
 3. 项目进度
 - [x] 个人中心及功能
 - [x] 注册
 4. 源码阅读
## 2020.9.2
 1. 文章阅读
 - [正则表达式](https://juejin.cn/post/6844903845227659271)
 2. 算法题
 - [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)
 - [整数反转](https://leetcode-cn.com/problems/reverse-integer/)
 3. 项目进度
 - [x] list列表功能
 - [x] 详细布局
 - [X] 封装面包屑组件
 4. 源码阅读
## 2020.9.1
 1. 文章阅读
 - [Vue 之keep-alive的使用，实现页面缓存](https://juejin.cn/post/6844904005508792333)
 2. 算法题
 - [子集](https://leetcode-cn.com/problems/subsets/submissions/)
 - [杨辉三角](https://leetcode-cn.com/problems/pascals-triangle/)
 3. 项目进度
 - [x] iframe功能
 - [x] 评论列表
 - [X] 封装footer组件
 4. 源码阅读
## 2020.8.31
 1. 文章阅读
 - [前端webSocket实现聊天消息&心跳检测&断线重连](https://juejin.cn/post/6914113426436390926)
 2. 算法题
 - [丑数](https://leetcode-cn.com/problems/ugly-number/)
 3. 项目进度
 - [x] 登录态，鉴权
 - [x] 工作台页面数据渲染
 - [X] 登陆后返回退出登陆页面
 4. 源码阅读
## 2020.8.30
 1. 文章阅读
 - [mobx 的原理以及在 React 中应用](https://juejin.cn/post/6979127131104083976)
 2. 算法题
 - [有效的数独](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2f9gg/)
 3. 项目进度
 - [x] 登录功能
 - [x] mobx状态管理的
 4. 源码阅读
## 2020.8.29
 1. 文章阅读
 - [防抖节流](https://juejin.cn/post/6844903736196726798)
 2. 算法题
 - [旋转数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2skh7/)
 - [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)
 3. 项目进度
 - [x] 构建项目框架
 - [x] 配置路由
 4. 源码阅读
## 2020.8.27
 1. 文章阅读
 - [vue中的data为什么是个函数](https://www.jianshu.com/p/f3e774c57356)
 2. 算法题
 - [反转字符串 II](https://leetcode-cn.com/problems/reverse-string-ii/submissions/)
 - [最大连续 1 的个数](https://leetcode-cn.com/problems/max-consecutive-ones/)
 3. 项目进度
 - [x] 路由懒加载
 - [x] hash配置
 4. 源码阅读
## 2020.8.26
 1. 文章阅读
 - [防抖节流](https://juejin.cn/post/6844903736196726798)
 2. 算法题
 - [救生艇](https://leetcode-cn.com/problems/boats-to-save-people/submissions/)
 - [生成平衡数组的方案数](https://leetcode-cn.com/problems/ways-to-make-a-fair-array/)
 3. 项目进度
 - [x] 路由懒加载
 - [x] hash配置
 4. 源码阅读
## 2020.8.25
 1. 文章阅读
 - [强缓存 协商缓存](https://juejin.cn/post/6844903736196726798)
 2. 算法题
 - [ 实现 strStr()](https://leetcode-cn.com/problems/implement-strstr/)
 - [Excel表列名称](https://leetcode-cn.com/problems/excel-sheet-column-title/)
 3. 项目进度
 - [X]路由懒加载
 - [X]项目埋点
 - [X]监听项目Error
 4. 源码阅读
## 2020.8.24
 1. 文章阅读
 - [React Hooks常用API源码解析](https://juejin.cn/post/6867459287720361991)
 2. 算法题
 - [ 搜索插入位置](https://leetcode-cn.com/problems/search-insert-position/submissions/)
 - [盛最多水的容器](https://leetcode-cn.com/problems/container-with-most-water/);
 3. 项目进度
 - [X]全局回到顶部
 - [X]项目上线
 4. 源码阅读
## 2020.8.23
 1. 文章阅读
 - [react-hooks原理](https://juejin.cn/post/6944863057000529933)
 2. 算法题
 - [主要元素](https://leetcode-cn.com/problems/find-majority-element-lcci/);
 - [ 最大子序和](https://leetcode-cn.com/problems/maximum-subarray/)
 3. 项目进度
 - [X]搜索功能
 4. 源码阅读
 - [hook源码](https://juejin.cn/post/6844904080758800392)
## 2020.8.22
 1. 文章阅读
 - [vue虚拟DOM](https://juejin.cn/post/6951239016004091911)
 - [es6语法糖](https://juejin.cn/post/6844903841717026830)
 2. 算法题
 - [加一](https://leetcode-cn.com/problems/plus-one/);
 - [最后一个单词的长度](https://leetcode-cn.com/problems/length-of-last-word/)
 3. 项目进度
 - [X]通用组件的优化
 - [X]组件拆分
 - [X]封装组件
## 2020.8.20
 1. 文章阅读
 - [CSS中的变量](https://juejin.cn/post/6989060618997694501)
 - [妙用CSS变量，让你的CSS变得更心动](https://juejin.cn/post/6844904084936327182)
 2. 算法题
 - [最长公共前缀](https://leetcode-cn.com/problems/longest-common-prefix/submissions/);
 - [移除元素](https://leetcode-cn.com/problems/remove-element/)
 3. 项目进度
 - [X]tag组件布局
 - [X]tag简单渲染
 - [X]封装组件
## 2020.8.19
 1. 文章阅读
 - [rem布局解析](https://juejin.cn/post/6844903671143088136)
 - [CSS中的尺寸单位](https://juejin.cn/post/6844903481690554381)
 2. 算法题
 - [整数反转](https://leetcode-cn.com/problems/reverse-integer/)
 - [删除有序数组中的重复项](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array/)
 3. 项目进度
 - [X]响应式布局
 - [X]详情页图片处理
 - [X]头部火柴棍处理 
## 2020.8.18
 1. 文章阅读
 - [umijs中的国际化](https://umijs.org/zh-CN/plugins/plugin-locale)
 - [console 篇 - console.log 的 "bug" ?](https://juejin.cn/book/6844733783166418958/section/6844733783208361998)
 2. 算法题
 - [z字变形](https://leetcode-cn.com/problems/zigzag-conversion/)
 - [有效的括号](https://leetcode-cn.com/problems/valid-parentheses/)
 3. 项目进度
 - [X]文章页国际化处理
 - [X]文章页跳转详情
 - [X]拆分组件
## 2021.8.16
 1. 文章阅读
 - [跨域资源共享——CORS](https://juejin.cn/post/6844903721613131789)
 - [Jsonp](https://www.jianshu.com/p/88bb82718517)
 2. 算法题
 - [ 打家劫舍](https://leetcode-cn.com/problems/house-robber/)
 - [回文数](https://leetcode-cn.com/problems/palindrome-number/)
 3. 项目进度
 - [X]主页数据渲染
 - [X]主页右侧数据渲染
## 2020.8.17
 1. 文章阅读
 - [元素面板篇 - 技巧集合](https://juejin.cn/book/6844733783166418958/section/6844733783216766989)
 - [Network 篇 - Network 的骚操作](https://juejin.cn/book/6844733783166418958/section/6844733783216750605)
 2. 算法题
 - [买卖股票的最佳时机](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2zsx1/)
 - [两个数组的交集 II](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2y0c2/)
 3. 项目进度
 - [X]主页标签块渲染
 - [X]主页下方路由跳转
## 2021.8.16
 1. 文章阅读
 - [跨域资源共享——CORS](https://juejin.cn/post/6844903721613131789)
 - [Jsonp](https://www.jianshu.com/p/88bb82718517)
 2. 算法题
 - [ 打家劫舍](https://leetcode-cn.com/problems/house-robber/)
 - [回文数](https://leetcode-cn.com/problems/palindrome-number/)
 3. 项目进度
 - [X]主页数据渲染
 - [X]主页右侧数据渲染
## 2021.8.15
1. 文章阅读
 - [React Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html#%E7%AE%80%E4%BB%8B)
 - [Umijs](https://umijs.org/zh-CN/docs)
2. 算法题
 - [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)
 - [买卖股票的最佳时机 II](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2zsx1/)
3. 项目进度
 - [X]主页构建项目
 - [X]一级路由
 - [X]首页轮播图
## 2021.8.13
1. 文章阅读
 - [console 篇 - console 中的 '$'](https://juejin.cn/book/6844733783166418958/section/6844733783208361991)
 - [Network 篇 - Network 的骚操作](https://juejin.cn/book/6844733783166418958/section/6844733783216750605)
2. 算法题
 - [无重复字符的最长子串](https://leetcode-cn.com/problems/longest-substring-without-repeating-characters/)
 - [寻找两个正序数组的中位数](https://leetcode-cn.com/problems/median-of-two-sorted-arrays/) 
3. 项目进度
 - [X] 初始化构建项目
## 2021.8.12
1. 文章阅读
 [呕心沥血编写的React Hooks最佳实践](https://juejin.cn/book/6844733783166418958)
2. 算法题
 [两数之和](https://leetcode-cn.com/problems/two-sum/)
 [无重复字符的最长子串](https://leetcode-cn.com/problems/longest-substring-without-repeating-characters/)
## 2021.8.11
1. 文章阅读
 [你不知道的 Chrome 调试技巧](https://juejin.cn/book/6844733783166418958)
2. 刷题
 [只出现一次的数字](https://leetcode-cn.com/leetbook/read/top-interview-questions/xm0u83/)
 [多数元素](https://leetcode-cn.com/leetbook/read/top-interview-questions/xm77tm/)


# 张泽轩
## 2021.9.15
1. 文章阅读
- [面试总结](https://gitlab.com/zzxzzx12313/when/-/blob/main/%E9%9D%A2%E8%AF%95.md)
2. leecode 刷题
- [分发饼干](https://leetcode-cn.com/problems/assign-cookies/)
- [重复的子字符串](https://leetcode-cn.com/problems/repeated-substring-pattern/)
3. 项目进度
- [x] 使用vue3.0和小程序 uni-app
- [x] 新开了一个项目:美味不用等
## 2021.9.13
1. 文章阅读
- [美团大佬解读的vuex源码](https://tech.meituan.com/2017/04/27/vuex-code-analysis.html)
2. leecode 刷题
- [z字变形](https://leetcode-cn.com/problems/zigzag-conversion/)
3. 源码解读
```
1. index.js 入口文件只做了一件事，就是导入了其他相关的文件，并且将vuex的功能export出去，相当于定义vuex对外使用的API
2. util.js 是封装了一些vuex源码中用到的一些公用方法
3. store.js 是vuex源码最重要的一环，是关于仓库的实现方法
    先是定义局部 Vue 变量，用于判断是否已经装载和减少全局作用域查找。
    在封装一个store类，最后又封装了12个在store.js 里用的函数
    最后抛出一个intall函数

```

## 2021.9.13
1. 文章阅读
- [宏烨找房](https://www.sqfcw.com/m/)
2. leecode 刷题
- [计算质数](https://leetcode-cn.com/problems/count-primes/)
- [阶乘后的零](https://leetcode-cn.com/problems/factorial-trailing-zeroes/)
3. 项目进度
- [x] 使用vue3.0和小程序 uni-app
- [x] 新开了一个项目:宏烨找房
## 2021.9.12
1. 文章阅读
- [if 我是前端团队 Leader，怎么制定前端协作规范?](https://juejin.cn/post/6844903897610321934)
2. leecode 刷题
 - [第三大的数](https://leetcode-cn.com/problems/third-maximum-number/)
 - [字符串相加](https://leetcode-cn.com/problems/add-strings/)
## 2021.9.10
1. 文章阅读
- [uni-app](https://uniapp.dcloud.io/)
2. leecode 刷题
 - [ 打家劫舍](https://leetcode-cn.com/problems/house-robber/)
 - [z字变形](https://leetcode-cn.com/problems/zigzag-conversion/)
3. 项目进度
- [x] 使用umijs和mobx
- [x] 写了搜索统计页面的数据渲染和基本排版

## 2021.9.9
1. 文章阅读
- [oss](https://help.aliyun.com/product/31815.html)
2. leecode 刷题
- [字符串中的第一个唯一字符](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn5z8r/)
- [两数之和 II - 输入有序数组](https://leetcode-cn.com/problems/two-sum-ii-input-array-is-sorted/)
3. 项目进度
- [x] 使用umijs和mobx
- [x] 写了搜索统计页面的数据渲染和基本排版

## 2021.9.8
1. 文章阅读
- [egg](https://eggjs.org/zh-cn/intro/quickstart.html)
2. leecode 刷题
- [反转字符串](https://leetcode-cn.com/problems/reverse-string/)
- [各位相加](https://leetcode-cn.com/problems/add-digits/)
## 2021.9.5
1. 文章阅读
- [元素面板篇 - 技巧集合](https://juejin.cn/book/6844733783166418958/section/6844733783216766989)
2. leecode 刷题
 - [有效的完全平方数](https://leetcode-cn.com/problems/valid-perfect-square/)
 - [赎金信](https://leetcode-cn.com/problems/ransom-note/)
## 2021.9.3
1. 文章阅读
- [react.next](https://www.nextjs.cn/)
2. leecode 刷题
 - [最大连续 1 的个数](https://leetcode-cn.com/problems/max-consecutive-ones/)
 - [多数元素](https://leetcode-cn.com/leetbook/read/top-interview-questions/xm77tm/)
3. 项目进度
- [x] 使用umijs和mobx
- [x] 完善了知识小册页面的基本功能和排版渲染

## 2021.9.2
1. 文章阅读
- [react.next](https://www.nextjs.cn/)
2. leecode 刷题
 - [子集](https://leetcode-cn.com/problems/subsets/submissions/)
 - [杨辉三角](https://leetcode-cn.com/problems/pascals-triangle/)
3. 项目进度
- [x] 使用umijs和mobx
- [x] 完善了知识小册页面的基本功能和排版渲染
## 2021.9.2
1. 文章阅读
- [react.next](https://www.nextjs.cn/)
2. leecode 刷题
- [多数元素](https://leetcode-cn.com/problems/majority-element/)
- [两数之和](https://leetcode-cn.com/problems/two-sum/)
3. 项目进度
- [x] 使用umijs和mobx
- [x] 完成了知识小册页面的基本功能和排版渲染
- [x] 开始写搜索页面
## 2021.9.1
1. 文章阅读
- [antd](https://ant.design/index-cn)
2. leecode 刷题
- [4的幂](https://leetcode-cn.com/problems/power-of-four/)
- [反转字符串](https://leetcode-cn.com/problems/reverse-string/)
3. 项目进度
- [x] 使用umijs和mobx
- [x] 完成了知识小册页面的一些排版和渲染数据
## 2021.8.31
1. 文章阅读
- [运行时配置](https://umijs.org/zh-CN/docs/runtime-config)
2. leecode 刷题
- [计算质数](https://leetcode-cn.com/problems/count-primes/)
- [多数元素](https://leetcode-cn.com/problems/two-sum-ii-input-array-is-sorted/)
3. 项目进度
- [x] 使用umijs和mobx
- [x] 完成了知识小册页面的一些排版和渲染数据
## 2021.8.30
1. 文章阅读
- [运行时配置](https://umijs.org/zh-CN/docs/runtime-config)
2. leecode 刷题
- [单词规律](https://leetcode-cn.com/problems/word-pattern/)
- [3的幂](https://leetcode-cn.com/problems/power-of-three/)
3. 项目进度
- [x] 使用umijs和mobx
- [x] 完成了知识小册页面的一些排版和获取数据
## 2021.8.29
1. 文章阅读
- [路由](https://jasonandjay.github.io/study/zh/standard/Spa.html#mpa%E4%B8%8Espa%E7%AE%80%E4%BB%8B)
2. leecode 刷题
- [丢失的数字](https://leetcode-cn.com/problems/missing-number/)
- [移动零](https://leetcode-cn.com/problems/move-zeroes/)
3. 项目进度
- [x] 新开了一个项目，和组长一起完成了路由的配置和icon的渲染
## 2021.8.27
1. 文章阅读
- [路由](https://jasonandjay.github.io/study/zh/standard/Spa.html#mpa%E4%B8%8Espa%E7%AE%80%E4%BB%8B)
2. leecode 刷题
- [各位相加](https://leetcode-cn.com/problems/add-digits/)
- [丑数](https://leetcode-cn.com/problems/ugly-number/)
## 2021.8.26
1. 文章阅读
- [路由](https://jasonandjay.github.io/study/zh/standard/Spa.html#mpa%E4%B8%8Espa%E7%AE%80%E4%BB%8B)
2. leecode 刷题
- [计算质数](https://leetcode-cn.com/problems/count-primes/)
- [同构字符串](https://leetcode-cn.com/problems/isomorphic-strings/)
## 2021.8.25
1. 文章阅读
- [缓存](https://jasonandjay.github.io/study/zh/standard/Cache.html#%E5%9B%BE%E8%A7%A3)
2. leecode 刷题
- [阶乘后的零](https://leetcode-cn.com/problems/factorial-trailing-zeroes/)
- [快乐数](https://leetcode-cn.com/problems/happy-number/)
## 2021.8.24
1. 文章阅读
- [hooks的钩子](https://leetcode-cn.com/problems/majority-element/)
2. leecode 刷题
- [多数元素](https://leetcode-cn.com/problems/two-sum-ii-input-array-is-sorted/)
- [Excel 表列序号](https://leetcode-cn.com/problems/excel-sheet-column-number/)
3. 项目进度
- [x] 使用服务器来打包上线了小楼又清风项目，还有很多要完善的地方
## 2021.8.23
1. 文章阅读
- [hooks的钩子](https://jasonandjay.github.io/study/zh/standard/Hooks.html#%E5%B8%B8%E7%94%A8hooks)
2. leecode 刷题
- [两数之和 II - 输入有序数组](https://leetcode-cn.com/problems/two-sum-ii-input-array-is-sorted/)
- [Excel表列名称](https://leetcode-cn.com/problems/excel-sheet-column-title/)
3. 项目进度
- [x] 使用UmiJS+ts完善了项目里面的知识小册页面的详情页面里面的双层id跳转的详情的排版和数据渲染
## 2021.8.22
1. 文章阅读
- [umijs路由](https://umijs.org/zh-CN/docs/routing)
2. leecode 刷题
- [二叉树的中序遍历](https://leetcode-cn.com/problems/binary-tree-inorder-traversal/)
- [买卖股票的最佳时机](https://leetcode-cn.com/problems/best-time-to-buy-and-sell-stock/)
3. 项目进度
- [x] 使用UmiJS+ts完善了项目里面的知识小册页面的详情页面里面的双层id跳转的详情的排版和数据渲染
## 2021.8.20
1. 文章阅读
- [umijs路由](https://umijs.org/zh-CN/docs/routing)
2. leecode 刷题
- [二进制求和](https://leetcode-cn.com/problems/add-binary/)
- [x的平方根](https://leetcode-cn.com/problems/sqrtx/)
3. 项目进度
- [x] 使用UmiJS+ts写了项目里面的知识小册页面的详情页面里面的双层id跳转的详情
## 2021.8.19
1. 文章阅读
- [umijs路由](https://umijs.org/zh-CN/docs/routing)
2. leecode 刷题
- [最后一个单词的长度](https://leetcode-cn.com/problems/length-of-last-word/)
- [加一](https://leetcode-cn.com/problems/plus-one/)
3. 项目进度
- [x] 使用UmiJS+ts完善了项目里面的知识小册页面的详情页面的排版和功能
## 2021.8.18
1. 文章阅读
- [umijs/plugin-locale](https://umijs.org/zh-CN/plugins/plugin-locale)
2. leecode 刷题
- [实现 strStr()](https://leetcode-cn.com/problems/implement-strstr/)
- [搜索插入位置](https://leetcode-cn.com/problems/search-insert-position/)
3. 源码阅读
- [dva源码](https://github.com/dvajs/dva)
4. 项目进度
- [x] 使用UmiJS+ts写了项目里面的知识小册页面的跳转详情页面
## 2021.8.17
1. 文章阅读
- [Network 篇 - Network 的骚操作](https://juejin.cn/book/6844733783166418958/section/6844733783216750605)
2. leecode 刷题
- [两个数组的交集 II](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2y0c2/)
- [只出现一次的数字](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/)
3. 项目进度
- [x] 使用UmiJS+ts 完善了项目里面的知识小册页面的数据渲染和精细排版
## 2021.8.16
1. 文章阅读
- [如何解决跨域问题](https://jasonandjay.github.io/study/zh/book/Ajax%E7%AF%87.html#%E5%A6%82%E4%BD%95%E8%A7%A3%E5%86%B3%E8%B7%A8%E5%9F%9F%E9%97%AE%E9%A2%98)
- [什么是版本控制系统（VCS）](https://juejin.cn/book/6844733697996881928/section/6844733698026242056)
2. leecode 刷题
- [有效的括号](https://leetcode-cn.com/problems/valid-parentheses/)
- [移除元素](https://leetcode-cn.com/problems/remove-element/)
3. 项目进度
- [x] 使用UmiJS+ts 完善了项目里面的知识小册页面的数据渲染和精细排版
## 2021.8.15
1. 文章阅读
- [@umijs/plugin-dva](https://umijs.org/zh-CN/plugins/plugin-dva)
- [知识小册](https://blog.wipi.tech/knowledge)
2. leecode 刷题
- [整数反转](https://leetcode-cn.com/problems/reverse-integer/)
- [回文数](https://leetcode-cn.com/problems/palindrome-number/)
3. 项目进度
- [x] 使用UmiJS+ts 完成了项目里面的知识小册页面的排版
## 2021.8.13
1. 文章阅读
- [小楼又清风知识小册](https://blog.wipi.tech/knowledge)
- [时间转换](http://momentjs.cn/docs/#/manipulating/)
2. leecode 刷题
- [罗马数字转整数](https://leetcode-cn.com/problems/roman-to-integer/)
- [最长公共前缀](https://leetcode-cn.com/problems/longest-common-prefix/)
3. 项目进度
- [x] 使用UmiJS+ts 完成了项目里面的知识小册页面的数据获取并渲染
## 2021.8.12
1. 文章阅读
- [项目开发流程](https://jasonandjay.github.io/study/zh/standard/Project.html#%E5%9B%BE%E8%A7%A3)
- [UmiJS](https://umijs.org/zh-CN)
2. leecode 刷题
- [两数之和](https://leetcode-cn.com/problems/two-sum/)
- [删除有序数组中的重复项 每个元素 最多出现两次](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array-ii/)
3. 项目进度
- [x] 使用UmiJS+ts 完成一个简易的TodoList
## 2021.8.11
1. 文章阅读
- [你不知道的 Chrome 调试技巧](https://juejin.cn/book/6844733783166418958/section/6844733783204167687)
- [快速搭建开发环境-三大环境](https://jasonandjay.github.io/study/zh/standard/Start.html#%E5%BF%AB%E9%80%9F%E6%90%AD%E5%BB%BA%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83)
2. leecode 刷题
- [队列和栈好](https://leetcode-cn.com/leetbook/read/queue-stack/xkrhpg/)

# 蒋瑞航
## 2021.9.9
1. 文章阅读
- [antd](https://ant.design/index-cn)
2. 源码阅读

3. leecode 刷题
- [买卖股票的最佳时机](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2zsx1/)
4. 项目进度
- [x] 封装文件管理页面组件
- [x] 邮件管理页面
- [x] 系统管理页面排版
- [x] 系统管理页面的系统设置页面
- [x] 国际化设置页面
## 2021.9.8
1. 文章阅读
- [antd](https://ant.design/index-cn)
2. 源码阅读

3. leecode 刷题
- [删除有序数组中的重复项 每个元素 最多出现两次](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array-ii/)
4. 项目进度
- [x] 评论管理回复功能
- [x] 封装用户管理页面组件
- [x] 文件管理页面
- [x] 封装文件管理页面组件
- [x] 邮件管理页面
- [x] 系统管理页面排版
## 2021.9.7
1. 文章阅读
 - []](https://es6.ruanyifeng.com/#docs/promise)
2. 源码阅读

3. leecode 刷题
- [删除有序数组中的重复项 每个元素 最多出现两次](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array-ii/)
4. 项目进度
- [X] 邮箱管理的搜索
- [X] 用户管理的搜索
- [x] 完成用户管理页面基本功能
- [x] 封装用户管理页面组件
- [x] 文件管理页面
- [x] 封装文件管理页面组件
- [x] 邮件管理页面
## 2021.9.6
1. 文章阅读
 - [promise](https://es6.ruanyifeng.com/#docs/promise)
2. 源码阅读

3. leecode 刷题
- [ 搜索插入位置](https://leetcode-cn.com/problems/search-insert-position/submissions/)
4. 项目进度
- [X] 邮箱管理的搜索
- [X] 用户管理的搜索
- [x] 完成用户管理页面基本功能
- [x] 封装用户管理页面组件
## 2021.9.3
1. 文章阅读
 - [EsLint](https://eslint.org/docs/user-guide/getting-started)
 - [iframe](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/iframe)
2. 源码阅读

3. leecode 刷题
- [罗马数字转整数](https://leetcode-cn.com/problems/roman-to-integer/)
- [ 搜索插入位置](https://leetcode-cn.com/problems/search-insert-position/submissions/)
4. 项目进度
- [X] 邮箱管理的搜索
- [X] 用户管理的搜索
- [x] 完成用户管理页面基本功能
## 2021.9.2
1. 文章阅读
 - [EsLint](https://eslint.org/docs/user-guide/getting-started)
 - [websocket.io](https://www.npmjs.com/package/websocket.io)
2. 源码阅读

3. leecode 刷题
- [罗马数字转整数](https://leetcode-cn.com/problems/roman-to-integer/)
4. 项目进度
- [x]鼠标滑过显示滚动条
- [x] 封装组件
- [x] 完成评论管理
- [X] 邮箱管理的搜索
- [X] 用户管理的搜索
## 2021.9.1
1. 文章阅读
 - [jQuery](https://jquery.cuishifeng.cn/)
 - [websocket.io](https://www.npmjs.com/package/websocket.io)
2. 源码阅读

3. leecode 刷题
- [罗马数字转整数](https://leetcode-cn.com/problems/roman-to-integer/)
4. 项目进度
- [x] 评论管理页面搜索框
- [x] table 表格
- [x] 鼠标滑过出现内容
- [x]鼠标滑过显示滚动条
- [x] 封装组件
- [x] 完成评论管理的基本功能
## 2021.8.31
1. 文章阅读
 - [jQuery](https://jquery.cuishifeng.cn/)
- [UmiJs](https://umijs.org/docs/getting-started)
2. 源码阅读

3. leecode 刷题
 - [最大连续 1 的个数](https://leetcode-cn.com/problems/max-consecutive-ones/)
4. 项目进度
- [x] 评论管理页面搜索框
- [x] table 表格
- [x] 鼠标滑过出现内容
- [x]鼠标滑过显示滚动条

## 2021.8.30
1. 文章阅读

- [UmiJs](https://umijs.org/docs/getting-started)
2. 源码阅读

3. leecode 刷题
- [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)
4. 项目进度
- [x] 评论管理页面搜索框
- [x] table 表格
- [x] 鼠标滑过出现内容
## 2021.8.29
1. 文章阅读
- [promise](https://es6.ruanyifeng.com/#docs/promise)
- [UmiJs](https://umijs.org/docs/getting-started)
2. 源码阅读

3. leecode 刷题
- [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)
4. 项目进度
- [x] 搭建框架
- [x] 路由搭建
- [x] 评论管理页面搜索框

## 2021.8.27
1. 文章阅读
- [promise](https://es6.ruanyifeng.com/#docs/promise)
2. 源码阅读

3. leecode 刷题
- [移动0](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnx13t/)
4. 项目进度
- [x] 优化代码
- [x] 表情功能完善BUG
- [x] 路由懒加载
- [x] hash配置
- [x] 打包上线
## 2021.8.26
1. 文章阅读
- [单页面应用开发](https://jasonandjay.github.io/study/zh/standard/Spa.html#%E4%BB%A3%E7%A0%81%E5%AE%9E%E7%8E%B0)
2. 源码阅读

3. leecode 刷题
- [整数反转](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnx13t/)
4. 项目进度
- [x] 优化代码
- [x] 表情功能完善BUG
- [x] 路由懒加载
- [x] hash配置
- [x] 打包上线
## 2021.8.25
1. 文章阅读
- [浏览器缓存](https://jasonandjay.github.io/study/zh/standard/Cache.html#%E5%8D%8F%E5%95%86%E7%BC%93%E5%AD%98%E7%9B%B8%E5%85%B3%E7%9A%84header%E5%AD%97%E6%AE%B5)
2. 源码阅读

3. leecode 刷题
- [移动0](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)
4. 项目进度
- [x] 优化代码
- [x] 表情功能完善BUG
- [x] 路由懒加载
- [x] hash配置
- [x] 打包上线
## 2021.8.24
1. 文章阅读
- [React Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)
2. 源码阅读

3. leecode 刷题
- [字符串中的第一个唯一字符](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn5z8r/)
4. 项目进度
- [x] 封装组件
- [x] 修复BUG
- [x] 优化代码
- [x] 表情功能
- [x] 点击标签存入输入框
## 2021.8.23
1. 文章阅读
- [React Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)
2. 源码阅读

3. leecode 刷题
- [买卖股票的最佳时机](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2zsx1/)
4. 项目进度
- [x] 封装组件
- [x] 修复BUG
- [x] 表情功能完成3分之二
- [x] 优化代码
## 2021.8.22
1. 文章阅读
- [你不知道的 Chrome 调试技巧](https://juejin.cn/book/6844733783166418958)
2. 源码阅读

3. leecode 刷题
- [加一](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2cv1c/)
4. 项目进度
- [x] 封装组件
- [x] 修复BUG
- [x] 表情功能完成3分之一
## 2021.8.20
1. 文章阅读
- [你不知道的 Chrome 调试技巧](https://juejin.cn/book/6844733783166418958)
2. 源码阅读

3. leecode 刷题
- [移动0](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)
4. 项目进度
- [x] 封装组件
- [x] 修复BUG
## 2021.8.19
1. 文章阅读
- [你不知道的 Chrome 调试技巧](https://juejin.cn/book/6844733783166418958)
- [图解HTTP彩色版](没有链接)
- [面试题](没有链接)
2. 源码阅读

3. leecode 刷题
- [旋转数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2skh7/)
4. 项目进度
- [x] 数据渲染
- [x] 分页
- [x] 评论数据渲染
- [x] 评论功能
- [x] 返回顶部
- [x] 回复的隐藏显示
- [x] 回复功能
- [x] 关于页面
- [x] 跳详情
## 2021.8.18
1. 文章阅读
- [你不知道的 Chrome 调试技巧](https://juejin.cn/book/6844733783166418958)
- [图解HTTP彩色版](没有链接)
- [面试题](没有链接)
2. 源码阅读
3. leecode 刷题
- [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5//)

4. 项目进度
- [x] 完成留言板页面排版
- [x] 数据渲染
- [x] 分页
- [x] 评论数据渲染
- [x] 评论功能
- [x] 返回顶部
- [x] 回复的隐藏显示
## 2021.8.17
1. 文章阅读
- [你不知道的 Chrome 调试技巧](https://juejin.cn/book/6844733783166418958)
- [图解HTTP彩色版](没有链接)
2. 源码阅读
3. leecode 刷题
- [实现 strStr](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnr003/)

4. 项目进度
- [x] 完成留言板页面排版
- [x] 数据渲染
- [x] 分页
- [x] 评论数据渲染

## 2021.8.16
1. 文章阅读
- [jsonp](https://zhuanlan.zhihu.com/p/24390509)
- [CORS](https://juejin.cn/post/6844904055148380173)
2. 源码阅读
3. leecode 刷题
- [字符串中的第一个唯一字符](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn5z8r/)

4. 项目进度
- [x] 构建项目结构
- [x] 基本配置
- [x] 完成留言板页面排版
- [x] 数据渲染
## 2021.8.15
1. 文章阅读
- [React Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)
- [UmiJS](https://umijs.org/docs/getting-started)
2. 源码阅读
3. leecode 刷题
- [翻转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2uudv/)

4. 项目进度
- [x] 构建项目结构
- [x] 基本配置
- [x] 完成留言板页面排版

## 2021.8.13
1. 文章阅读
- [React Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)
- [UmiJS](https://umijs.org/docs/getting-started)
2. 源码阅读
3. leecode 刷题
- [旋转数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2skh7/)
- [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)
4. 项目进度
- [x] 构建项目结构
- [x] 基本配置
## 2021.8.12
1. 文章阅读
- [React Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)
2. 源码阅读
3. leecode 刷题
- [买卖股票的最佳时机](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2zsx1/)
- [只出现一次的数字](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/)
4. 项目进度
## 2021.8.11
1. 文章阅读
- [你不知道的 Chrome 调试技巧](https://juejin.cn/book/6844733783166418958/section/6844733783187390477)
- [git教程](https://blog.csdn.net/weixin_42152081/article/details/80558282?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162864253116780255257035%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=162864253116780255257035&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-1-80558282.first_rank_v2_pc_rank_v29&utm_term=git&spm=1018.2226.3001.4187)
2. 源码阅读
3. leecode 刷题
- [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)
- [只出现一次的数字](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/)
4. 项目进度

# 杨富
## 2021.9.9
1. 文章阅读
- [沙箱](https://open.alipay.com/platform/home.htm?from=wwwalipay)
2. 源码阅读
3. leecode 刷题
- [买卖股票的最佳时机 II](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2zsx1/)
4. 项目进度
- [x] 页面优化
- [x] 编辑页面优化
## 2021.9.8
1. 文章阅读
- [Generator](https://es6.ruanyifeng.com/#docs/generator)
2. 源码阅读
3. leecode 刷题
- [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)
4. 项目进度
- [x] 页面优化
## 2021.9.7
1. 文章阅读
- [Generator](https://es6.ruanyifeng.com/#docs/generator)
2. 源码阅读
3. leecode 刷题
- [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)
4. 项目进度
- [x] 标签页面
- [x] 编辑页面更新
- [x] 新建页面完结
## 2021.9.6
1. 文章阅读
- [Generator](https://es6.ruanyifeng.com/#docs/generator)
2. 源码阅读
3. leecode 刷题
- [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)
4. 项目进度
- [x] 标签页面
- [x] 编辑页面更新
## 2021.9.3
1. 文章阅读
- [Umijs](https://umijs.org/zh-CN/docs)
2. 源码阅读
3. leecode 刷题
- [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2cv1c/)
4. 项目进度
- [x] 文章管理渲染
- [x] 分类页面
- [x] 标签页面
## 2021.9.2
1. 文章阅读
- [EsLink](https://eslint.org/)
- [Umijs](https://umijs.org/zh-CN/docs)
2. 源码阅读
3. leecode 刷题
- [加一](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2cv1c/)
4. 项目进度
- [x] 文章管理渲染
- [x] 编辑页面渲染
- [x] mobx数据状态管理
## 2021.9.1
1. 文章阅读
- [promise](https://es6.ruanyifeng.com/#docs/promise)
- [Umijs](https://umijs.org/zh-CN/docs)
2. 源码阅读
3. leecode 刷题
- [两个数组的交集 II](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2y0c2/)
4. 项目进度
- [x] 文章管理渲染
- [ ] websocket (报错)
- [x] mobx数据状态管理

## 2021.8.31
1. 文章阅读
- [jQuery 教程](https://www.w3school.com.cn/jquery/index.asp)
- [Umijs](https://umijs.org/zh-CN/docs)
2. 源码阅读
3. leecode 刷题
- [两个数组的交集 II](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2y0c2/)
4. 项目进度
- [x] 文章管理渲染
- [ ] websocket (报错)
- [x] mobx数据状态管理
## 2021.8.30
1. 文章阅读
- [react hooks](https://react.docschina.org/docs/hooks-intro.html)
- [Umijs](https://umijs.org/zh-CN/docs)
2. 源码阅读
3. leecode 刷题
- [有效的数独](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2f9gg/)
4. 项目进度
- [x] 文章管理渲染
- [x] mobx数据状态管理
## 2021.8.29
1. 文章阅读
- [react hooks](https://react.docschina.org/docs/hooks-intro.html)
- [Umijs](https://umijs.org/zh-CN/docs)
2. 源码阅读
3. leecode 刷题
- [有效的数独](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2f9gg/)
4. 项目进度
- [x] 路由搭建
- [x] 文章管理渲染
## 2021.8.27
1. 文章阅读
- [宏任务和微任务](https://react.docschina.org/docs/hooks-intro.html)
- [promise](https://es6.ruanyifeng.com/#docs/promise#%E5%9F%BA%E6%9C%AC%E7%94%A8%E6%B3%95)
2. 源码阅读
3. leecode 刷题
- [旋转图像](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2f9gg/)
4. 项目进度
- 
## 2021.8.26
1. 文章阅读
- [react hooks](https://react.docschina.org/docs/hooks-intro.html)
- [单页面应用](https://jasonandjay.github.io/study/zh/standard/Spa.html#history404%E9%87%8D%E5%AE%9A%E5%90%91%E8%A7%A3%E5%86%B3)
2. 源码阅读
3. leecode 刷题
- [旋转图像](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2f9gg/)
4. 项目进度
- [x] 分享组件
- [x] 代码优化
## 2021.8.25
1. 文章阅读
- [react hooks](https://react.docschina.org/docs/hooks-intro.html)
- [浏览器缓存](https://jasonandjay.github.io/study/zh/standard/Cache.html#%E5%8D%8F%E5%95%86%E7%BC%93%E5%AD%98%E7%9B%B8%E5%85%B3%E7%9A%84header%E5%AD%97%E6%AE%B5)
2. 源码阅读
3. leecode 刷题
- [有效的数独](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2f9gg/)
4. 项目进度
- [x] 楼层效果优化
- [x] 分享下载生成海报
- [x] 分享组件
- [x] 代码优化
## 2021.8.24
1. 文章阅读
- [react hooks](https://react.docschina.org/docs/hooks-intro.html)
2. 源码阅读
3. leecode 刷题
- [两数之和](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2jrse/)
4. 项目进度
- [x] 楼层效果优化
- [x] 分享下载生成海报
- [x] 分享组件
- [x] 代码优化
## 2021.8.23
1. 文章阅读
- [react hooks](https://react.docschina.org/docs/hooks-intro.html)
2. 源码阅读
3. leecode 刷题
- [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)
4. 项目进度
- [x] 点赞加评论
- [x] 封装点赞组件目录
- [x] 分享组件
- [x] 代码优化
## 2021.8.22
1. 文章阅读
- [react hooks](https://react.docschina.org/docs/hooks-intro.html)
2. 源码阅读
3. leecode 刷题
- [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)
4. 项目进度
- [x] 点赞加评论
- [x] 封装点赞组件目录
## 2021.8.21
1. 文章阅读
- [react hooks](https://react.docschina.org/docs/hooks-intro.html)
2. 源码阅读
3. leecode 刷题
- [加一](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2cv1c/)
4. 项目进度
- [x] 点赞加评论
- [x] 楼层左右互动
- [x] 封装组件目录
## 2021.8.20
1. 文章阅读
- [react hooks](https://react.docschina.org/docs/hooks-intro.html)
- [Umijs](https://umijs.org/zh-CN/docs)
2. 源码阅读
3. leecode 刷题
- [加一](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2cv1c/)
4. 项目进度
- [x] 跳详情页
- [x] 楼层
- [x] 封装组件
## 2021.8.19
1. 文章阅读
- [react hooks](https://react.docschina.org/docs/hooks-intro.html)
- [Umijs](https://umijs.org/zh-CN/docs)
2. 源码阅读
3. leecode 刷题
- [加一](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2cv1c/)
4. 项目进度
- [x] 跳详情页
- [x] 楼层
- [x] 封装组件
## 2021.8.18
1. 文章阅读
- [Chremo](https://jasonandjay.github.io/study/zh/standard/Hooks.html#%E7%AE%80%E4%BB%8B)
- [Umijs](https://umijs.org/zh-CN/docs)
2. 源码阅读
3. leecode 刷题
- [两个数组的交集 II](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2y0c2/)
4. 项目进度
- [x] 跳详情页
- [x] 数据渲染
- [x] 封装组件
## 2021.8.17
1. 文章阅读
- [Chremo](https://jasonandjay.github.io/study/zh/standard/Hooks.html#%E7%AE%80%E4%BB%8B)
- [Umijs](https://umijs.org/zh-CN/docs)
2. 源码阅读
3. leecode 刷题
- [两个数组的交集 II](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2y0c2/)
4. 项目进度
- [ ] 全局排版
- [x] 数据渲染
- [x] 封装组件
## 2021.8.16
1. 文章阅读
- [Chremo](https://jasonandjay.github.io/study/zh/standard/Hooks.html#%E7%AE%80%E4%BB%8B)
- [Umijs](https://umijs.org/zh-CN/docs)
- [jsonp](https://juejin.cn/post/6844903976505344013)
2. 源码阅读
3. leecode 刷题
- [两个数组的交集 II](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2y0c2/)
4. 项目进度
- [x] 全局排版
- [x] 数据渲染
- [x] 封装组件
## 2021.8.15
1. 文章阅读
- [React Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html#%E7%AE%80%E4%BB%8B)
- [Umijs](https://umijs.org/zh-CN/docs)
2. 源码阅读
3. leecode 刷题
- [只出现一次的数字](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/)
4. 项目进度
- [x] 全局排版
- [x] 响应式布局
- [x] 获取页面数据加部分渲染
## 2021.8.13
1. 文章阅读
- [React Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)
- [UmiJS](https://umijs.org/docs/getting-started)
2. 源码阅读
3. leecode 刷题
- [旋转数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2skh7/)
- [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)
4. 项目进度
- [x] 构建项目结构
- [x] 基本配置
## 2021.8.12
1. 文章阅读
- [React Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html#%E7%AE%80%E4%BB%8B)
- [Umijs](https://umijs.org/zh-CN/docs)
2. 源码阅读
3. leecode 刷题
- [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)
- [买卖股票的最佳时机 II](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2zsx1/)
4. 项目进度
## 2021.8.11
1. 文章阅读
- [chrome调试](https://juejin.cn/book/6844733783166418958/section/6844733783187390477)
- [git官网教程](https://blog.csdn.net/weixin_44950987/article/details/102619708?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162864385316780357234023%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=162864385316780357234023&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-2-102619708.first_rank_v2_pc_rank_v29&utm_term=git%E6%95%99%E7%A8%8B&spm=1018.2226.3001.4187)
- [git pull 和 git fetch 的区别](https://blog.csdn.net/weixin_41975655/article/details/82887273)

3. leecode 刷题
4. 项目进度
